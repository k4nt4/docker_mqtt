## Mosquitto

Eclipse Mosquitto is an open source (EPL/EDL licensed) message broker that implements the MQTT protocol versions 3.1 and 3.1.1.
Mosquitto is lightweight and is suitable for use on all devices from low power single board computers to full servers (see https://mosquitto.org/).

## Dockerfile

Based on debian jessie. Port 1883 is configuered as default port. Port 8080 as Websocket-Port.
Basic authentication is enabled by passwd file and anonymous clients are disabled.
The passwd file should be located on local machine. 

## Container

```docker
docker run -it -p --restart=always --name=mqtt 1883:1883 -p 8080:8080 -v /path/on/host:/etc/mosquitto/password/ kanta/mosquitto
```

## First Start

Open CMD line and create a new password file

```cmd
mosquitto_passwd -c /ect/mosquitto/password/password User
```

## Add new User

```cmd
mosquitto_passwd /etc/mosquitto/password/password NewUser
```

